---
title: Linux内核学习资料
type: docs
---

# Linux内核资料汇总

## Linux内核同步机制

[Linux内核同步机制之（一）：原子操作](www.wowotech.net/kernel_synchronization/atomic.html)

[Linux内核同步机制之（二）：Per-CPU变量](http://www.wowotech.net/kernel_synchronization/per-cpu.html)

[Linux中的spinlock机制[一] - CAS和ticket spinlock](https://zhuanlan.zhihu.com/p/80727111)

[Linux中的spinlock机制[二] - MCS Lock](https://zhuanlan.zhihu.com/p/89058726)

[Linux中的spinlock机制[三] - qspinlock](https://zhuanlan.zhihu.com/p/100546935)

[Linux中的spinlock机制[四] - API的使用](https://zhuanlan.zhihu.com/p/90634198)

[Linux内核同步机制之（八）：mutex](http://www.wowotech.net/kernel_synchronization/504.html)
