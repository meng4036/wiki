

# 安装cloudflared

参考链接： [Set up a tunnel locally](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/get-started/create-local-tunnel/)


从github下载cloudflared文件
https://hub.nuaa.cf/cloudflare/cloudflared/releases

## CLI创建tunnel

### 创建tunnel
    # 登录cloudflared
    $ cloudflared tunnel login

    # 创建tunnel
    $ cloudflared tunnel create <NAME>

    # 查看tunnel状态
    $ cloudflared tunnel list

### 创建配置文件
url: http://localhost:8000
tunnel: <Tunnel-UUID>
credentials-file: /root/.cloudflared/<Tunnel-UUID>.json

### 使能路由
    $ cloudflared tunnel dns <UUID or NAME> <hostname>
hostname是待绑定的域名,如redmine.freeiot.top

### 运行tunnel
    $ cloudflared tunnel run <UUID or NAME>

这种方式，tunnel运行在前台，不是后台服务。如果需要后台运行，需要自己编写服务。


## Web控制台创建tunnel

参考资料： [Set up a tunnel through the dashboard](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/get-started)